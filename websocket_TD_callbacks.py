# -- This file is part of Livecoding_TouchDesigner_websocket -- 
# Copyright 2019 B.E. Laurencich & L.M. Ferri from Chordata

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#   http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
NAME_API_MODULE = "remote_api"

api_module = op(NAME_API_MODULE)
target = op("target_script")

accepted_types = ["command", "panel"]

def error_message(reason):
  return json.dumps({"type": "error", "payload": reason})

def success_message():
  return json.dumps({"type": "success", "payload": None})

def run_command(cmd):
  target.text = "# The contents of this DAT are automatically generated\n"
  target.text += "# ------------- DO NOT EDIT MANUALLY !! --------------\n\n"
  target.text += "from %s import * \n\n" % NAME_API_MODULE

  target.text += cmd

  target.text += "\n\nsetup()"

  try:
    run_ob = target.run()
    return ("success", run_ob)

  except Exception as e:
    return ("error", str(e))
    print("[ERROR]", e)




# me - this DAT
# dat - the DAT that received a message
# rowIndex - the row number the message was placed into
# message - an ascii representation of the text
# 
# Only text frame messages will be handled in this function.
def onReceiveText(dat, rowIndex, message):
  message_dict = {}
  try:
    message_dict = json.loads(message)

    if "type" not in message_dict.keys() or message_dict["type"] not in accepted_types:
      dat.sendText(error_message("incorrect 'type' in messge"))
      raise "incorrect 'type' in messge"
    elif "payload" not in message_dict.keys():  
      dat.sendText(error_message("incorrect 'payload' in messge"))
      raise "incorrect 'payload' in messge"

  except Exception as e:
    dat.sendText(error_message("malformed message:\n %s" % e))
    print("malformed message:\n %s" % e)
    return

    
  if message_dict["type"] == "command":
    result = run_command(message_dict["payload"])
    if result[0] == "success":
      dat.sendText(success_message())
    else:
      dat.sendText(error_message("{}\nTD-python Runtime Error:\n %{} \n{}\n\n".format("~"*20 ,result[1], "~"*20)))  
  elif message_dict["type"] == "panel":
    op('panel').text = message_dict["payload"]
  else:
    print("-- no known type message received --")
  
  return

# me - this DAT
# dat - the DAT that received a message
# message - an ascii representation of the message
#
# Use this method to monitor the websocket status messages
def onMonitorMessage(dat, message):
  print("[MONITOR] " , message)
  if message.find("Switching Protocols") >= 0:
    response = {
      "type": "initial_module",
      "name": NAME_API_MODULE,
      "payload": api_module.text 
      }

    dat.sendText(json.dumps(response))
      
  return



