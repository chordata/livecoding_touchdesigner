# -- This file is part of Livecoding_TouchDesigner_websocket -- 
# Copyright 2019 B.E. Laurencich & L.M. Ferri from Chordata

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import json
websocet_dat = op("websocket1")
from sys import modules 
import td

ERROR_REPORT_INTERVAL = 2

def error_message(reason):
 return json.dumps({"type": "error", "payload": reason})

try:
    import target_script
except Exception as e:
    websocet_dat.sendText(error_message("== ERROR IN TARGET_SCRIPT == \n %s" % e))

last_time = 0

def onDone(timerOp, segment, interrupt):
    global last_time
    this_time = td.absTime.seconds 
    try:
        target_script.loop()
    except Exception as e:
        if this_time - ERROR_REPORT_INTERVAL > last_time:
            websocet_dat.sendText(
            	error_message("=== ERROR IN LOOP @ ={0:06.2f} {2}> (!) \n {1}"
            		.format(this_time, e, "=" * int(this_time/ERROR_REPORT_INTERVAL % 12))))
            last_time = this_time

    return