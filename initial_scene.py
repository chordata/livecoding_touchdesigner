# -- This file is part of Livecoding_TouchDesigner_websocket -- 
# Created by Chordata NewArts
# Source at https://gitlab.com/chordata/livecoding_touchdesigner

delay = 60

polygons(MAX_BETA_POLYS, delay)

noise(25, delay)

red_light(1, delay)

white_light(0, delay/2)
