#!/usr/bin/python3

# -- This file is part of Livecoding_TouchDesigner_websocket -- 
# Copyright 2019 B.E. Laurencich & L.M. Ferri from Chordata

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#   http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
import json
import traceback

accepted_types = ["initial_module", "success", "error"]
default_file = "perform.py" 
file_polling_interval = 0.2
tmux_min_interval = 0.1


def command_message(cmd):
    return json.dumps({"type":"command", "payload": cmd})

def panel_message(content):
    return json.dumps({"type":"panel", "payload": content})

def write_remote_api_module(msg):
    with open(msg["name"]+".py", "w") as f:
        f.write(msg["payload"])

def read_commands_file(name):
    with open(name, "r") as f:
        return f.read()

_conected_clients = []

class LiveCodingTDServer(WebSocket):

    def handleMessage(self):
        message_dict = {}
        try:
            message_dict = json.loads(self.data)
            if "type" not in message_dict.keys() or message_dict["type"] not in accepted_types:
                raise "incorrect 'type' key on message_dict"
            elif "payload" not in message_dict.keys():
                raise "incorrect 'payload' key on message_dict"

        except Exception as e:
            print("malformed message_dict:\n", self.data)
            return
        
        try:

            if message_dict["type"] == "initial_module":
                write_remote_api_module(message_dict)
                print(" == Received TD API module. Saved to [%s.py] ==" % message_dict["name"])
                return

            if message_dict["type"] == "success":
                print("Command success!")

            if message_dict["type"] == "error":
                print("COMMAND ERROR:\n", message_dict["payload"])    
            
        except Exception as e:
            traceback.print_exc()


    def handleConnected(self):
        _conected_clients.append(self)
        print(self.address, 'connected')
        

    def handleClose(self):
        _conected_clients.remove(self)
        print(self.address, 'closed')


class closingWebSocketServer(SimpleWebSocketServer):
    def __init__(self, host, port, websocketclass, selectInterval = 0.1):
        super().__init__(host, port, websocketclass, selectInterval)
        self.is_running = True

    def close(self):
        self.is_running = False
        super().close()

import signal
import threading
import time
import os
import sys
import subprocess
import argparse
from pynput import keyboard

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = 'Websocket server used on a Touchdesigner live performance')
    parser.add_argument("--pane", "-p", nargs="?", default="%0", help="The tmux pane to capture")
    parser.add_argument("target_file", nargs="?", default=default_file, help="File to monitor for changes")
    parser_args = parser.parse_args()

    the_file = parser_args.target_file

    print(" >> Polling file:", the_file)
    print(" >> Tmux pane:", parser_args.pane)

    port = 9595
    server = closingWebSocketServer('', port, LiveCodingTDServer)

    last_keystroke = time.time()
    def on_key_release(key):
        # global last_keystroke
        # print(time.time() - last_keystroke)
        # if time.time() - last_keystroke < tmux_min_interval:
        #     return
        # print("sending")
        last_keystroke = time.time()
        args = ['tmux', 'capture-pane', "-J", "-p", "-t", parser_args.pane]
        result = subprocess.run(args, stdout=subprocess.PIPE)
        for client in _conected_clients:       
            client.sendMessage(panel_message(result.stdout.decode("utf-8")))

    key_listen = keyboard.Listener(on_release=on_key_release)
    key_listen.start()

    def poll_and_send(file):
        last_edit = os.stat(file).st_mtime
        while server.is_running:
            try:
                this_edit = os.stat(file).st_mtime
                
                if this_edit > last_edit:
                    print("File changed at: ", time.ctime(this_edit))
                    last_edit = this_edit
                    for client in _conected_clients:       
                        client.sendMessage(command_message(read_commands_file(file)))

            except Exception as e:
                print("Error Polling File: ", e)

            time.sleep(file_polling_interval)
        
        print("polling thread terminating...")
    
    t = threading.Thread(target=poll_and_send, args=(the_file,))
    t.start()
    
    def close_sig_handler(signal, frame):
        print("\nExit..")
        key_listen.stop()
        server.close()
        sys.exit()

    signal.signal(signal.SIGINT, close_sig_handler)
    

    print("WebSocket server started on ws://0.0.0.0:%d" % port)
    server.serveforever()
    t.join()
    key_listen.join()
    # t_tmux.join()
