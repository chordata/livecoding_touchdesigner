# Simple Websocket framework for remote livecoding in Touchdesigner

![livecoding_TD_perform](screenshot_perform.jpg)


- Written in python
- TD .tox included
- Dependencies (python libs included as git submodules): 
	- POSIX compatible terminal with TMUX and console text editor.
	- [SimpleWebSocketServer](https://github.com/dpallot/simple-websocket-server/) python library
	- [Pynput](https://github.com/moses-palmer/pynput) python library

It is based on a python Websocket server that polls for changes on a text file and sends them to TD.
It also listens for keystrokes on your OS, and sends the contents of a TMUX pane to TD. That way you can screencast your coding session


## Usage
### Create the WS server

First download the libraries as submodules

```bash
git submodule init
git submodule update
```

From a remote PC run the `live_coding.py` script. It will create the server and wait for the WS client on the port 9595.
by default it will poll the file `perform.py` for changes, and capture the contents of the tmux pane %0. 

Or run the following for more options 
```bash
live_coding.py -h
``` 

### Import the component in Touchdesigner
Import the `remote_livecoding.tox` on your TD project and connect it to a textDAT

![livecoding_tox](screenshot_livecoding_tox.png)

Inside the container look for the `websocket1` operator, set the IP of the remote PC, and pulse the _Active_ button.

### LiveCode!
If the connection was sucessful you should see a message on the server side.

Now you can use the contents of the textDAT whereever you like, they will always be updated with the last changes on the server's pane.

### API
The way the code is structured was inspired by [Processing](https://processing.org/). 
You can write statements directly on the body of your `perform.py`, or inside a
```python
def setup():
	#one-go statements here
```
To execute them right away

Or, if you want to have an ongoing interaction with the TD operators you can put them inside a `loop()` function

```python
def loop():
	#looping statements here
```
It will be executed once on every TD frame. Be carefull with what you write here since it may have a significant impact on your framerate.

**Always keep in mind that while python code is running the TD cooking cascade stops**, so it is better to only include precise modifications to some operator's parameters

## EXAMPLE:

```python
#This example assumes that you have a light with a Null parent
#It will make the light spin while turning it on and off  

#Since we are executing this inside a container, is handy to have a 
#reference to the global project, where most of our operators live 
pr = op("/project1")

#A global variable will allow us to get some persistency
#between loop() calls
foo = 0

def loop():
	global foo
	foo += 1
	pr.op("lights_parent").par.ry = foo 
	if foo % 60 == 0:
		pr.op("light1").par.dimmer = 0
	if foo % 60 == 30:
		pr.op("light1").par.dimmer = 1

```

## Hints:
The full contents of the `perform.py` script will be executed inside the container created when importing the `.tox`. So you will have to reference the project container, or make relative calls to get your operators.

Before injecting the contents of `perform.py`, all the elements from a module called `remote_api` will be imported using
```python
from remote_api import *
```
That module lives inside the `remote_livecoding.tox`, so you can fill it with wrappers or convenience functions.
For example the `pr = op("/project1")` statement is there by default, so you can start using the variable `pr` it on your `perform.py` without declaring it.  

## LICENSE:
Distributed under the Apache 2.0 license.



