# -- This file is part of Livecoding_TouchDesigner_websocket -- 
# Created by Chordata NewArts
# Source at https://gitlab.com/chordata/livecoding_touchdesigner

delay = 30

polygons(0.1 * MAX_BETA_POLYS, delay)

noise(0.2, delay)

red_light(0, delay)

white_light(0, delay/2)
foo = 0
def loop():
	global foo
	foo +=1
	op("lights_parent").par.ry = foo
	if foo % 60 == 0:
		feedback(42, .2)
		wireframe(0)
	if foo % 60 == 30:
		feedback(85, .2)
		wireframe(1)
